﻿
class Cromossome
{
    public int[] cromossome;
    public static Random rng = new Random();
    public Cromossome()
    {
        cromossome = new int[10];
        for (int i = 0; i < 10; i++)
        {
            cromossome[i] = rng.Next(2);
        }
    }

    public void Mutate()
    {
        if (rng.Next(2) == 1)
        {
            int idx = rng.Next(10);
            cromossome[idx] = Math.Abs(1 - cromossome[idx]);
        }
    }
    
    public float Fitness()
    {
        float sum = 0;
        float prod = 1;
        for (int i = 1; i < 11; i++)
        {
            if (cromossome[i - 1] == 0) sum += i;
            if (cromossome[i - 1] == 1) prod *= i;
        }

        return 1 / (1 + MathF.Abs(sum-36) + MathF.Abs(prod-360) / 10);
    }

    public override string ToString()
    {
        string repr = "";
        for (int i = 0; i < 10; i++)
            repr += $"{cromossome[i]} ";
        return repr;
    }
}

class Program
{
    static void Main()
    {
        // 1. Criar populacao inicial
        int pop_size = 200;
        List<Cromossome> population = new List<Cromossome>();
        for (int i = 0; i < pop_size; i++)
            population.Add(new Cromossome());
        
        // 2. Iterar n gerações
        int geration_count = 100;
        for (int generation = 0; generation < geration_count; generation++)
        {
            List<Cromossome> new_population = new List<Cromossome>();
            while (new_population.Count != pop_size)
            {
                // Selecionar Progenitores - Tournament
                Cromossome pai = Fight(population);
                Cromossome mae = Fight(population);
                // Realizar Cruzamento
                Tuple<Cromossome, Cromossome> filhos = Cross(pai, mae);
                // Realizar Mutacao
                filhos.Item1.Mutate();
                filhos.Item2.Mutate();
                // Adicionar filhos na nova populacao
                new_population.Add(filhos.Item1);
                new_population.Add(filhos.Item2);
            }
            // Elitismo (hard-coded)
            new_population[0] = new_population.MaxBy(c => c.Fitness());
            // Iterar sobre nova população.
            population = new_population;
            Console.WriteLine($"Generation {generation}");
        }

        Cromossome fittest = population.MaxBy(c => c.Fitness());
        Console.WriteLine(fittest);
        Console.WriteLine(fittest.Fitness());
    }

    //  PAI  1 0 0 | 1 1 0 1 1 0 1
    //  MAE  0 1 1 | 0 0 0 1 1 1 1
    
    // FILHO1  1 0 0 | 0 0 0 1 1 1 1 
    // FILHO2  0 1 1 | 1 1 0 1 1 0 1
    static Tuple<Cromossome, Cromossome> Cross(Cromossome a, Cromossome b)
    {
        Cromossome child_a = new Cromossome();
        Cromossome child_b = new Cromossome();
        int cut_point = Cromossome.rng.Next(11);
        for (int i = 0; i < 10; i++)
        {
            if (i < cut_point)
            {
                child_a.cromossome[i] = a.cromossome[i];
                child_b.cromossome[i] = b.cromossome[i];
            }
            else
            {
                child_a.cromossome[i] = b.cromossome[i];
                child_b.cromossome[i] = a.cromossome[i];
            }
        }

        return new Tuple<Cromossome, Cromossome>(child_a, child_b);
    }
    
    static Cromossome Fight(List<Cromossome> population)
    {
        Cromossome a = population[Cromossome.rng.Next(population.Count)];
        Cromossome b = population[Cromossome.rng.Next(population.Count)];
        float fit_a = a.Fitness();
        float fit_b = b.Fitness();
        // TODO: dada uma probabilidade muito baixa, selecionar o pior
        return fit_a > fit_b ? a : b;
    }
}