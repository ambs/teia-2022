﻿using GeneticSharp.Domain.Chromosomes;
using System;

namespace ex2
{
    class MyProblemChromosome : FloatingPointChromosome
    {
        public MyProblemChromosome() : base(
            new []{ 0.0, 0.0, 0.0, 0.0 },
            new [] {1000.0, 500.0, 1000.0, 500.0},
            new[] {20, 20, 20, 20},
            new [] { 2, 2, 2, 2})
        {
        }
        
        public override IChromosome CreateNew()
        {
            return new MyProblemChromosome();
        }
    }
}
