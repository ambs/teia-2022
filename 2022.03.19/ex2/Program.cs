﻿using GeneticSharp.Domain;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Populations;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using System;
using GeneticSharp.Domain.Chromosomes;

namespace ex2
{
    class Program
    {
        /// <summary>
        /// GeneticSharp Console Application template.
        /// <see href="https://github.com/giacomelli/GeneticSharp"/>
        /// </summary>
        ///
        ///  1 2 3 4 | 3 4 5 6 | 7 8 9
        ///  1 3 5 2 | 5 4 6 2 | 4 6 7
        ///
        ///  1 2 3 4 5 4 6 2 7 8 9
        ///  1 3 5 2 3 4 5 6 4 6 7
        static void Main(string[] args)
        {
            // TODO: use the best genetic algorithm operators to your optimization problem.
            var selection = new TournamentSelection(); // new EliteSelection();
            var crossover = new TwoPointCrossover();
            var mutation = new UniformMutation(true);

            var fitness = new MyProblemFitness();
            var chromosome = new MyProblemChromosome();

            var population = new Population(50, 70, chromosome);

            var ga = new GeneticAlgorithm(population, fitness, selection, crossover, mutation);
            ga.Termination = new FitnessStagnationTermination(100);
            ga.GenerationRan += (s, e) =>
                {
                    FloatingPointChromosome c = ga.BestChromosome as FloatingPointChromosome;
                    double[] points = c.ToFloatingPoints();
                    Console.WriteLine($" ({points[0]}, {points[1]}) ({points[2]}, {points[3]})");
                    Console.WriteLine(
                        $"Generation {ga.GenerationsNumber}. Best fitness: {ga.BestChromosome.Fitness.Value}");
                };

            Console.WriteLine("GA running...");
            ga.Start();

            Console.WriteLine();
            Console.WriteLine($"Best solution found has fitness: {ga.BestChromosome.Fitness}");
            Console.WriteLine($"Elapsed time: {ga.TimeEvolving}");
            Console.ReadKey();
        }
    }
}
