﻿using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Fitnesses;
using System;

namespace ex2
{
    class MyProblemFitness : IFitness
    {
        public double Evaluate(IChromosome chromosome)
        {
            FloatingPointChromosome c = chromosome as FloatingPointChromosome;
            double[] points = c.ToFloatingPoints();
            double x1 = points[0];
            double y1 = points[1];
            double x2 = points[2];
            double y2 = points[3];
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }
    }
}
